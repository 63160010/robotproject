/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robotprojects;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        TableMap map=new TableMap(10,10);
        Robot robot=new Robot(2,2,'r',map);
        Bomb bomb=new Bomb(6,6);
        map.setRobot(robot);
        map.setBomb(bomb);
        while(true){
            map.showMap();
            char direction=inputDirection(sc);
            if(direction=='q'){
                printByeBye();
                break;
            }
           robot.walk(direction);
        }
        
    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner sc) {
        String str=sc.next();
        return str.charAt(0);
    }
}
